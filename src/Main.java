import ch.fhnw.dist.spamfilter.bur.Inbox;
import ch.fhnw.dist.spamfilter.bur.Mail;
import ch.fhnw.dist.spamfilter.bur.SpamFilter;

import java.util.Scanner;

public class Main {

    /**
     * Application Main Method
     * IMPORTANT: Run this method for this application!
     * @param args
     */
    public static void main(String[] args) {

        // a) Read Mails and analyze
        printUnit("a) Read Mails and analyze");
        SpamFilter.getInstance().alpha = 0.01;
        Inbox inbox1 = new Inbox();
        inbox1.collectAndAnalyze();

        // b) calculate SPAM possibility with a given mail text
        printUnit("b) Test specific message for Spam or Ham");
        inputSpamCheck(false);

        // c) change alpha variable
        printUnit("c) define threshold");
        SpamFilter.getInstance().threshold = 0.7;
        Inbox inbox2 = new Inbox("spam-kallibrierung.zip","ham-kallibrierung.zip");
        inbox2.collectAndCheck(true);

        // d) Test mails
        printUnit("d) test mails with threshold: " + SpamFilter.getInstance().threshold);
        Inbox inbox3 = new Inbox("spam-test.zip","ham-test.zip");
        inbox3.collectAndCheck(false);

        // e) Test & Learn with specific input
        inputSpamCheck(true);


    }

    /**
     * Method to enter Mail-text to check for Spam or Ham
     */
    public static void inputSpamCheck(boolean learn){
        Scanner scanner = new Scanner(System.in);
        String message = "";
        while(!message.equals("XX")){
            System.out.print("Enter a specific Mail-Text and continue with Enter or type [XX] to quit...");
            message = scanner.nextLine();
            Mail m = new Mail();
            m.textOnly = message; // "Craig potent";
            double res = m.calcSpam();
            System.out.println("Spam Possibility: \t" + Math.round(res * 100) + "%");
            if(learn){
                System.out.print("Is this a SPAM Mail? [y|n|XX]");
                message = scanner.nextLine();

                // Ad Word to filter list if "y" or "n"
                if(message.equals("y") || message.equals("n")){
                    String[] words = m.textOnly.split("\\s+");
                    for (int i = 0; i < words.length; i++) {
                        // remove non-word characters
                        words[i] = words[i].replaceAll("[^\\w]", "");
                    }
                    // Add each word into SpamFilter WordFilter Collection
                    for(String word : words){
                        // add word only once per mail
                        SpamFilter.getInstance().countWord(word,message.equals("y"));
                    }
                }
            }
        }
    }

    /**
     * Outprint for Command line
     * @param title
     */
    public static void printUnit(String title){
        System.out.println("\n\n--------------------------------------------------------------");
        System.out.println("-- " + title);
        System.out.println("--------------------------------------------------------------\n\n");

    }
}
