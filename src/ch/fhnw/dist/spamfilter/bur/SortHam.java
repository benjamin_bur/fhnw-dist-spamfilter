package ch.fhnw.dist.spamfilter.bur;

/**
 * Created by ben-g on 11.10.2015.
 */
import java.util.Comparator;
/**
 * Comparator for SpamFilterWords by ham
 */
public class SortHam implements Comparator<SpamFilterWord>{
    @Override
    public int compare(SpamFilterWord a1, SpamFilterWord a2) {
        if (a1.ham < a2.ham) return -1;
        if (a1.ham > a2.ham) return 1;
        return 0;
    }
}
