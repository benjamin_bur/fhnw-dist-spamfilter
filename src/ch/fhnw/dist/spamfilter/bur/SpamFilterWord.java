package ch.fhnw.dist.spamfilter.bur;

/**
 * Created by ben-g on 04.10.2015.
 */
public class SpamFilterWord {
    String word;
    double spam;
    double ham;

    /**
     * Calcs the Spam-Quantity for this SpamFilterWord +1
     */
    public void countSpam(){
        if(spam > 0 && spam <1)
            spam = 1.0;
        else
            spam+=1.0;
    }
    /**
     * Calcs the Ham-Quantity for this SpamFilterWord +1
     */
    public void countHam(){
        if(ham > 0 && ham <1)
            ham = 1.0;
        else
            ham+=1.0;
    }

    /**
     * Calcs the Ham or Spam-Quantity for this SpamFilterWord +1
     * @param isSpam
     */
    public void count(boolean isSpam){
        if(isSpam)
            countSpam();
        else
            countHam();
    }

    public SpamFilterWord(String w){
        this.word = w;
        // Get Alpha from SpamFilter - Class
        spam = SpamFilter.getInstance().alpha;
        ham = SpamFilter.getInstance().alpha;
    }

}
