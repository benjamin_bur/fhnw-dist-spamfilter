package ch.fhnw.dist.spamfilter.bur;

import java.util.ArrayList;

/**
 * Created by ben-g on 04.10.2015.
 */
public class SpamFilter {
    private static SpamFilter instance = null;
    public double alpha = 0.1;
    public double threshold = 0.0;
    private ArrayList<SpamFilterWord> spamFilterWords = new ArrayList<SpamFilterWord>();

    protected SpamFilter() {

    }

    /**
     * Clears the spamFilterWords Array List
     */
    public void clearMails(){ spamFilterWords = new ArrayList<SpamFilterWord>(); }

    /**
     * Singleton
     * @return SpamFilter
     */
    public static SpamFilter getInstance() {
        if(instance == null) {
            instance = new SpamFilter();
        }
        return instance;
    }

    /**
     * Returns the quantity of SpamFilterWords in the current SpamFilter
     * @return
     */
    public int getQuantity(){
        return spamFilterWords.size();
    }

    /**
     * Prints the statistics of the SpamFilter (each word is printed out)
     */
    public void printStatistic(){
        System.out.println("PRINT STATISTICS FROM " + spamFilterWords.size() + " WORDS...");
        for(SpamFilterWord spf : spamFilterWords){
            if(spf.spam > 5 || spf.ham > 5)
            System.out.println("Significant #Spam\t" + spf.spam + "\t\t#Ham:\t" + spf.ham + "\t\tWord: " + spf.word);
        }
    }

    /**
     * Returns a SpamFilterWord for a given word or null if not found.
     * @param w word to search for
     * @return SpamFilterWord or null if not found
     */
    public SpamFilterWord getSpamFilterWord(String w){
        for(SpamFilterWord spf : spamFilterWords){
            if(spf.word.equals(w))
                return spf;
        }
        return null;
    }

    /**
     * Method to add a new word and count ++spam or ++ham
     * or just count spam or ham if the word is already in
     * the wordfilterlist.
     * @param word
     * @param isSpam
     */
    public void countWord(String word,boolean isSpam){
        // check if word is a "real word" containing a-z and A-Z
        if(!word.matches ("[a-zA-Z]+"))
            return;

        // Count ++ if already in Word List
        for(int i=0; i<spamFilterWords.size(); i++){
            if(spamFilterWords.get(i).word.equals(word)){
                if(isSpam){
                    spamFilterWords.get(i).spam++;
                }else{
                    spamFilterWords.get(i).ham++;
                }
                return;
            }
        }

        // Add new FilterWord if not yet in Word List
        SpamFilterWord spf = new SpamFilterWord(word);
        spf.count(isSpam);
        spamFilterWords.add(spf);
        // System.out.println("word " + word + " added.");

    }
}
