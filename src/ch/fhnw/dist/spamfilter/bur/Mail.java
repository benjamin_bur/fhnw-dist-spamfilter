package ch.fhnw.dist.spamfilter.bur;

import org.jsoup.Jsoup;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by ben-g on 04.10.2015.
 */
public class Mail {
    public boolean isSpam;
    InputStream content;
    public String textOnly;

    public Mail(){}

    public Mail(boolean i,InputStream c){
        isSpam = i;
        content = c;
    }

    /**
     * Get the most significant words from the current mail.
     * 10 words with the biggest possibility of SPAM and
     * 10 words with the biggest possibility of HAM
     * @return ArrayList\<String\>
     */
    public ArrayList<String> mostSignificantWords(){
        String[] words = textOnly.split("\\s+");
        ArrayList<SpamFilterWord> wordList = new ArrayList<>();
        ArrayList<SpamFilterWord> wordListHam = new ArrayList<>();

        ArrayList<String> out = new ArrayList<>();
        for (int i = 0; i < words.length; i++) {
            // remove non-word characters
            words[i] = words[i].replaceAll("[^\\w]", "");
        }

        // only add the 10 most significant words (spam / ham) to the list
        for(String word : words){
            SpamFilterWord spf = SpamFilter.getInstance().getSpamFilterWord(word);
            if(spf != null) {
                wordList.add(spf);
                wordListHam.add(spf);
            }
        }

        Collections.sort(wordList, new SortSpam());
        int c = 0;
        for(SpamFilterWord s: wordList){
            if(c++ <10)
                out.add(s.word);
        }
        c=0;
        Collections.sort(wordListHam, new SortHam());
        for(SpamFilterWord s: wordListHam){
            if(c++ <10)
                out.add(s.word);
        }

        return out;
    }

    /**
     * This method calculates the possibility of SPAM in a Mail
     * @return double percent Value (for example 0.5 = 50%)
     */
    public double calcSpam(){
        int total = SpamFilter.getInstance().getQuantity();
        ArrayList<String> words = mostSignificantWords();

        double a = -1.0;
        double b = -1.0;

        for(String word : words){
            SpamFilterWord spf = SpamFilter.getInstance().getSpamFilterWord(word);
            if(spf!=null){
                double ham = spf.ham / total;
                double spam = spf.spam / total;
                if(a < 0){
                    a = spam;
                }else{
                    a*=spam;
                }
                if(b < 0){
                    b = ham;
                }else{
                    b*=ham;
                }
            }
        }
        return a/(a+b);
    }

    /**
     * This method is able to read a Mail and add words to the singleton SpamFilter class if
     * @addToFilterList is true.
     * @param addToFilterList
     * @throws IOException
     */
    public void readMail(boolean addToFilterList) throws IOException{
        //System.out.print("Reading " + ((isSpam) ? "SPAM" : "HAM") + " Mail...");

        ArrayList<String> mailWordList = new ArrayList<String>();

        int data = content.read();
        String text = "";

        while(data != -1) {
            text += (char) data;
            data = content.read();
        }
        content.close();

        // remove html tags
        textOnly = Jsoup.parse(text.toString()).text();

        if(addToFilterList){
            // get words from mail text
            String[] words = textOnly.split("\\s+");
            for (int i = 0; i < words.length; i++) {
                // remove non-word characters
                words[i] = words[i].replaceAll("[^\\w]", "");
            }

            // Add each word into SpamFilter WordFilter Collection
            for(String word : words){
                // add word only once per mail
                if(!mailWordList.contains(word)){
                    mailWordList.add(word);
                    SpamFilter.getInstance().countWord(word,isSpam);
                }
            }

            System.out.println("Collected " + words.length  + " words from " + ((isSpam)? "SPAM" : "HAM") + " Mail.");

           /* System.out.print(" done |");
            System.out.print(" collected " + words.length  + " words | ");

            System.out.println();*/
        }
    }
}
