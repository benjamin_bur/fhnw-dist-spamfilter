package ch.fhnw.dist.spamfilter.bur;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Created by ben-g on 04.10.2015.
 */
public class Inbox {
    public String FILE_SPAM = "spam-anlern.zip";
    public String FILE_HAM = "ham-anlern.zip";

    ArrayList<Mail> mails;

    public Inbox(String f1, String f2){
        mails = new ArrayList<Mail>();
        FILE_SPAM = f1;
        FILE_HAM = f2;
    }
    public Inbox(){
        mails = new ArrayList<Mail>();
    };


    /**
     * Check collected Mails for Spam or Ham
     * @param optimizeThreshold
     */
    public void collectAndCheck(boolean optimizeThreshold){
        // Check Mails and optimize threshold
        collectMails();
        checkMails(optimizeThreshold);
    }

    /**
     * This method collects mails and analyze them of SPAM or HAM.
     */
    public void collectAndAnalyze(){
        // Fill Mail-List with Mail-Objects
        collectMails();
        analyzeMails();

        SpamFilter.getInstance().printStatistic();
    }

    /**
     * Check Mails from ArrayList if Spam or not
     */
    public void checkMails(boolean fixThreshold){
        int correct = 0; int wrong = 0; int total= 0;
        for(Mail m: mails){
            try{
                m.readMail(false);
            }catch(IOException e){
                e.printStackTrace();
            }
            double spam = m.calcSpam();
            System.out.println(((m.isSpam)?"SPAM":"HAM") + " Mail: Spam possibility: " + spam);

            // correct only if spam value is bigger than 0.3
            if(fixThreshold && m.isSpam && spam > 0.3){
                // correct threshold
                if(spam < SpamFilter.getInstance().threshold){
                    // makes threshold lower if spam Value is in a range of 0.3 - 0.1 smaller
                    if(spam + 0.3 >= SpamFilter.getInstance().threshold)
                        SpamFilter.getInstance().threshold -= 0.03;
                    if(spam + 0.2 >= SpamFilter.getInstance().threshold)
                        SpamFilter.getInstance().threshold -= 0.02;
                    else if(spam + 0.1 >= SpamFilter.getInstance().threshold)
                        SpamFilter.getInstance().threshold -= 0.01;
                }
            }

            // Test Case
            if(!fixThreshold){
                double t = SpamFilter.getInstance().threshold;
                // Add result of Spam-Check to Result List
                total++;
                if(m.isSpam && spam >= t || !m.isSpam && spam < t){
                    correct++;
                }else{
                    wrong++;
                }
            }
        }
        if(!fixThreshold && total > 0){
            System.out.println("#correct: " + correct);
            System.out.println("#wrong: " + wrong);
            double res = (double)correct / ((double)correct + (double)wrong);
            System.out.println("(%) Correct: " + Math.round(res * 100) + "%");
        }
    }

    /**
     * Analyze each Mail in mails-ArrayList
     */
    public void analyzeMails(){
        for(Mail m: mails){
            try{
                m.readMail(true);
            }catch(IOException e){
                e.printStackTrace();
            }
        }
    }

    /**
     * Get Mails from a given ZIP-File and store
     * the mails into the local variable ->mails.
     */
    public void collectMails(){
        ZipFile zipFileSpam = null;
        ZipFile zipFileHam = null;
        try {
            zipFileSpam = new ZipFile(FILE_SPAM);
            zipFileHam = new ZipFile(FILE_HAM);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Read SPAM-ZIP-File -----------
        Enumeration<? extends ZipEntry> entries = zipFileSpam.entries();
        while(entries.hasMoreElements()){
            ZipEntry entry = entries.nextElement();
            try {
                // Get Content-Stream binary
                InputStream stream = zipFileSpam.getInputStream(entry);
                Mail m = new Mail(true,stream);
                // add to Mail-List
                mails.add(m);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // Read HAM-ZIP-File -----------
        entries = zipFileHam.entries();
        while(entries.hasMoreElements()){
            ZipEntry entry = entries.nextElement();
            try {
                // Get Content-Stream binary
                InputStream stream = zipFileHam.getInputStream(entry);
                Mail m = new Mail(false,stream);
                // add to Mail-List
                mails.add(m);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }
}
