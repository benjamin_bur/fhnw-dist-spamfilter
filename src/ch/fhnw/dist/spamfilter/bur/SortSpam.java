package ch.fhnw.dist.spamfilter.bur;

/**
 * Created by ben-g on 11.10.2015.
 */
import java.util.Comparator;
/**
 * Comparator for SpamFilterWords by spam
 */
public class SortSpam implements Comparator<SpamFilterWord>{
    @Override
    public int compare(SpamFilterWord a1, SpamFilterWord a2) {
        Double diff = a1.spam - a2.spam;
        return diff.intValue();
    }
}
